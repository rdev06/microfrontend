import * as React from 'react';
import PropTypes from 'prop-types';
import './ExampleComponent.css';

export class ExampleComponent extends React.Component {
  state = {
    checkInit:true
  };

  inputChange = () => {
    this.setState(prevState => ({ checkInit: !prevState.checkInit }));
  };

  static propTypes = {
    name: PropTypes.string,
    onHelloEvt: PropTypes.func
  }

  static defaultProps = {
    name: "Chris"
  }

  render() {
    const { name, onHelloEvt } = this.props;
    const { checkInit } = this.state;
    return (
      <div className={checkInit ? 'exampleComponent' : 'afterChange'}>
        <img src="/images/react.png" alt="React Logo" className="logo" />
        <input type='text' />
        <input type='checkbox' onChange={this.inputChange} />
        <p>Hello <strong>{name}</strong> from your friendly React component.</p>
        <button type="submit" className="btn btn-secondary" onClick={onHelloEvt}>Say hello</button>
      </div>
    )
  }
}
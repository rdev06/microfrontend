import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-getname',
  templateUrl: './getname.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None,
})
export class GetnameComponent implements OnInit {
  @Input() name: string;
  @Output() action = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}
  handleClick() {
    this.action.emit(this.name);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GetnameComponent } from './components/getname/getname.component';

@NgModule({
  declarations: [AppComponent, GetnameComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  // bootstrap: [AppComponent],
})
export class AppModule {
  constructor(private injector: Injector) {}
  ngDoBootstrap() {
    const customElement = createCustomElement(GetnameComponent, {
      injector: this.injector,
    });
    customElements.define('app-getname', customElement);
  }
}

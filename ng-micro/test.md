<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>NgMicro</title>
  <base href="./">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="styles.css">
</head>

<body>
  <app-getname name='roshan'></app-getname>
  <script src="runtime-es2015.js"></script>
  <script src="runtime-es5.js" nomodule defer></script>
  <script src="polyfills-es5.js" nomodule defer></script>
  <script src="polyfills-es2015.js"></script>
  <script src="main-es2015.js"></script>
  <script src="main-es5.js" nomodule defer></script>
  <script>
    const target = document.querySelector('app-getname');
    target.addEventListener('action', e => console.log(e.detail))
    setTimeout(() => {
      target.name = 'sourabh';
    }, 3000);
  </script>
</body>

</html>

# Micro-frontends with Web Components

Using web components as a wrapper for both Angular and React components, I am going to show them working alongside each other in a single application. I will also pass data to both components from the parent container, and back again.

Follow the instructions below to build and serve micro-frontend components for Angular and React, and a wrapper application to compose them together and handle communication between them.

to get started install all the dependencies from each folder
then do npm build and start
also install
npm i -g http-server

In there we create main html file in which we call angular component and react component. In our main html we write script or create function for lazyload in which we pass url for angular/react component.

We also create function loadAngular for append angular container and this function also do action event which is also define in our angular typescript file like
 @Output() action = new EventEmitter();
 
similarly for reactjs we create loadReact function and same work. 
